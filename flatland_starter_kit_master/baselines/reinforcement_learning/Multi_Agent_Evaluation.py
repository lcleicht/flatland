from datetime import datetime
import PIL
import torch
import numpy as np
import sys

from pathlib import Path
from rtpt import RTPT
from torch.utils.tensorboard import SummaryWriter
from flatland.utils.rendertools import RenderTool
from IPython.display import clear_output

from flatland.envs.rail_env import RailEnv
from flatland.envs.rail_generators import sparse_rail_generator
from flatland.envs.rail_generators import test_rail_generator
from flatland.envs.rail_generators import custom_rail_generator
from flatland.envs.line_generators import sparse_line_generator
from flatland.envs.line_generators import test_line_generator
from flatland.envs.line_generators import custom_line_generator
from flatland.envs.malfunction_generators import ParamMalfunctionGen
from flatland.envs.malfunction_generators import TestMalfunctionGen
from flatland.envs.observations import GlobalObsForRailEnv
from flatland.envs.planning_controllers import PlanningController
from flatland.envs.step_utils.states import TrainState

base_dir = Path(__file__).resolve().parent.parent
sys.path.append(str(base_dir))

from reinforcement_learning.dddqn_policy import DDDQNPolicy
from argparse import Namespace


def evaluate_agents(env_params):
    # Environment parameters
    n_agents = env_params.n_agents
    x_dim = env_params.x_dim
    y_dim = env_params.y_dim
    n_cities = env_params.n_cities
    max_rails_between_cities = env_params.max_rails_between_cities
    max_rail_pairs_in_city = env_params.max_rail_pairs_in_city
    seed = env_params.seed
    n_episodes = env_params.n_episodes
    render_episodes = env_params.render_episodes
    evaluation_model = env_params.evaluation_model
    if evaluation_model in range(6, 8):
        max_nb_conflicts = 7
    elif evaluation_model in range(8, 10):
        max_nb_conflicts = 10
    else:
        max_nb_conflicts = 4

    # Unique ID for this training
    now = datetime.now()
    eval_id = now.strftime('%y%m%d%H%M%S')

    # choose this RailEnv for random environments
    """
    eval_env = RailEnv(
        width=x_dim, height=y_dim,
        rail_generator=sparse_rail_generator(
            max_num_cities=n_cities,
            grid_mode=False,
            max_rails_between_cities=max_rails_between_cities,
            max_rail_pairs_in_city=max_rail_pairs_in_city
        ),
        line_generator=sparse_line_generator(),
        number_of_agents=n_agents,
        malfunction_generator=ParamMalfunctionGen((0, 0, 0)),
        obs_builder_object=GlobalObsForRailEnv(),
        random_seed=seed
    )
    """
    # choose this RailEnv for the double track
    """
    eval_env = RailEnv(width=x_dim, height=y_dim,
        rail_generator=test_rail_generator(
            max_num_cities=n_cities,
            grid_mode=False,
            max_rails_between_cities=max_rails_between_cities,
            max_rail_pairs_in_city=max_rail_pairs_in_city
        ),
        line_generator=test_line_generator(),
        number_of_agents=n_agents,
        malfunction_generator=TestMalfunctionGen((0, 0, 0)),
        obs_builder_object=GlobalObsForRailEnv(),
        random_seed=seed)
    """

    # choose this RailEnv for the custom grid
    eval_env = RailEnv(width=x_dim, height=y_dim,
        rail_generator=custom_rail_generator(),
        line_generator=custom_line_generator(),
        number_of_agents=n_agents,
        malfunction_generator=ParamMalfunctionGen((0, 0, 0)),
        obs_builder_object=GlobalObsForRailEnv(),
        random_seed=seed)

    if render_episodes:
        renderer = RenderTool(eval_env, gl="PILSVG")

    state_size = 10 * (2 * 3 * 30 + 1 + max_nb_conflicts * 4)
    action_size = 10

    # Initialisation of performance indicators
    eval_normalized_score = []
    eval_completion = []
    eval_nb_conflicts = []
    nb_valid_episodes = 0
    eval_reward = []

    policy = DDDQNPolicy(state_size, action_size, Namespace(**{'use_gpu': True}), evaluation_mode=True)

    if evaluation_model == 2:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220223123812-360.pth', map_location='cuda:0').to(policy.device)
    elif evaluation_model == 3:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220301020858-479.pth', map_location='cuda:0').to(policy.device)
    elif evaluation_model == 4:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220222234018-360.pth', map_location='cuda:0').to(policy.device)
    elif evaluation_model == 5:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220301015543-263.pth', map_location='cuda:0').to(policy.device)
    elif evaluation_model == 6:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220223122934-360.pth', map_location='cuda:0').to(policy.device)
    elif evaluation_model == 7:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220301015153-390.pth', map_location='cuda:0').to(policy.device)
    elif evaluation_model == 8:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220227130755-240.pth', map_location='cuda:0').to(policy.device)
    elif evaluation_model == 9:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220301014640-372.pth', map_location='cuda:0').to(policy.device)
    elif evaluation_model == 10:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220307233427-810.pth', map_location='cuda:0').to(policy.device)
    elif evaluation_model == 11:
        policy.qnetwork_local = torch.load('../flatland_starter_kit_master/baselines/checkpoints/220307233146-720.pth', map_location='cuda:0').to(policy.device)

    # TensorBoard writer
    writer = SummaryWriter()
    writer.add_hparams(vars(env_params), {})

    rtpt = RTPT(name_initials='LL', experiment_name='FlatlandEvaluation', max_iterations=50)
    rtpt.start()

    for episode_idx in range(1, n_episodes + 1):
        obs, info = eval_env.reset(random_seed=episode_idx)
        controller = PlanningController(eval_env)

        if render_episodes:
            renderer.set_new_rail()

        # skip the episode, if at least one train has no path from origin to destination
        if not controller.episode_valid:
            continue

        # Init these values after reset()
        max_steps = eval_env._max_episode_steps
        state = [None] * max_steps
        action = [None] * max_steps
        episode_valid = True
        score = 0
        nb_conflicts = 0

        # Run episode
        for step in range(max_steps):
            try:
                conflicts = controller.find_conflicts(eval_env)
            except:
                episode_valid = False
                break
            nb_conflicts += len(conflicts)
            for i in range(len(conflicts)):
                solutions = controller.find_solutions(conflicts[i], i, eval_env)
                state[i] = solutions_to_array(solutions, step, max_nb_conflicts)
                state[i].astype(float)
                if evaluation_model in range(2, 12):
                    action_value_array = policy.act(state[i], eps=0.0)[0]
                    sorted_index_array = (-action_value_array).argsort()[:]
                    j = 0
                    while j < action_size:
                        action[i] = sorted_index_array[j]
                        if action[i] is not None and solutions[action[i]] is not None:
                            break
                        else:
                            j += 1

                elif evaluation_model == 0:
                    action[i] = controller.always_choose_wait(conflicts[i], solutions)
                elif evaluation_model == 1:
                    action[i] = controller.choose_solution_manually(conflicts[i], solutions)

                if action[i] is not None and solutions[action[i]] is not None:
                    controller.choose_solution(conflicts[i], solutions[action[i]])

            action_dict = controller.act(obs, eval_env)

            # Environment step
            next_obs, all_rewards, done, info = eval_env.step(action_dict, step)
            obs = next_obs

            # Update the observations of the next state
            for agent in eval_env.get_agent_handles():
                score += all_rewards[agent]

            # Render an episode at some interval
            if render_episodes:
                render_env(eval_env, renderer)
                print('Timestep {}, total score = {}'.format(step, score))

            if done['__all__']:
                break

        # skip the episode, if there was an error
        if not episode_valid:
            continue

        # Collect information about training
        tasks_finished = sum([agent.state == TrainState.DONE for agent in eval_env.agents])
        completion = tasks_finished / max(1, eval_env.get_num_agents())
        normalized_score = score / max(1, nb_conflicts)

        eval_normalized_score.append(normalized_score)
        eval_completion.append(completion)
        eval_nb_conflicts.append(nb_conflicts)
        eval_reward.append(score)

        writer.add_scalar("evaluation/scores_mean", np.mean(eval_normalized_score), episode_idx)
        writer.add_histogram("evaluation/scores", np.array(eval_normalized_score), episode_idx)
        writer.add_scalar("evaluation/rewards_mean", np.mean(eval_reward), episode_idx)
        writer.add_histogram("evaluation/rewards", np.array(eval_reward), episode_idx)
        writer.add_scalar("evaluation/nb_conflicts_mean", np.mean(eval_nb_conflicts), episode_idx)
        writer.add_histogram("evaluation/nb_conflicts", np.array(eval_nb_conflicts), episode_idx)
        writer.add_scalar("evaluation/completions_mean", np.mean(eval_completion), episode_idx)
        writer.add_histogram("evaluation/completions", np.array(eval_completion), episode_idx)

        rtpt.step()


def render_env(env, renderer, wait=True):
    env_renderer = renderer
    env_renderer.render_env()

    image = env_renderer.get_image()
    pil_image = PIL.Image.fromarray(image)
    clear_output(wait=True)
    display(pil_image)


def solutions_to_array(solutions, step, max_nb_conflicts):
    result = np.array([])
    for i in range(len(solutions)):
        if solutions[i] is None:
            temp1 = np.full(30 * 3, 0)
            temp2 = np.full(30 * 3, 0)
            temp3 = np.full(1, 0)
            temp4 = np.full(4 * max_nb_conflicts, 0)

        else:
            temp1 = np.empty((30, 3))
            temp2 = np.empty((30, 3))
            temp3 = np.empty(1)
            temp4 = np.empty((max_nb_conflicts, 4))
            for j in range(30):
                if len(solutions[i][2]) <= step + j or solutions[i][2][step+j][0] is None:
                    temp1[j][0] = 0
                    temp1[j][1] = 0
                    temp1[j][2] = 0
                else:
                    temp1[j][0] = solutions[i][2][step+j][0][0]
                    temp1[j][1] = solutions[i][2][step+j][0][1]
                    temp1[j][2] = solutions[i][2][step+j][1]
                if len(solutions[i][3]) <= step + j or solutions[i][3][step+j][0] is None:
                    temp2[j][0] = 0
                    temp2[j][1] = 0
                    temp2[j][2] = 0
                else:
                    temp2[j][0] = solutions[i][3][step+j][0][0]
                    temp2[j][1] = solutions[i][3][step+j][0][1]
                    temp2[j][2] = solutions[i][3][step+j][1]
            temp1 = np.hstack(temp1)
            temp2 = np.hstack(temp2)
            temp3[0] = len(solutions[i][4])
            k = 0
            while k < len(solutions[i][4]):
                temp4[k][0] = solutions[i][4][k][0]
                temp4[k][1] = solutions[i][4][k][1]
                temp4[k][2] = solutions[i][4][k][2]
                temp4[k][3] = solutions[i][4][k][3]
                k += 1
                if k >= max_nb_conflicts:
                    break
            while k < max_nb_conflicts:
                temp4[k][0] = 0
                temp4[k][1] = 0
                temp4[k][2] = 0
                temp4[k][3] = 0
                k += 1
            temp4 = np.hstack(temp4)
        result = np.concatenate([result, temp1, temp2, temp3, temp4])
    return result.astype(float)


# nice trick from https://goodcode.io/articles/python-dict-object/ (accessed on 09.12.2021)
class objectview(object):
    def __init__(self, d):
        self.__dict__ = d
