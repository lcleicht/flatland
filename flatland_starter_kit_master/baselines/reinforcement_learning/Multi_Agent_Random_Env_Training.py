#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import sys
sys.path.insert(0, "../../../")

from flatland_starter_kit_master.baselines.reinforcement_learning import multi_agent_random_env


# In[ ]:


import os
os.chdir('../')


# nice trick from https://goodcode.io/articles/python-dict-object/ (accessed on 09.12.2021)
class objectview(object):
    def __init__(self, d):
        self.__dict__ = d


train_params_dict = {"n_episodes": 1200,
    "training_env_config": 0,
    "evaluation_env_config": 0,
    "n_evaluation_episodes": 0,
    "checkpoint_interval": 30,
    "eps_start": 1.0,
    "eps_end": 0.01,
    "eps_decay": 0.99,
    "buffer_size": int(1e5),
    "buffer_min_size": 0,
    "restore_replay_buffer": "",
    "save_replay_buffer": False,
    "batch_size": 128,
    "gamma": 0.99,
    "tau": 1e-3,
    "learning_rate": 0.5e-4,
    "hidden_size": 128,
    "update_every": 8,
    "use_gpu": True,
    "num_threads": 1,
    "render": False
               }
train_params = objectview(train_params_dict)

train_env_params_dict = {
            # Test_0
            "n_agents": 20,
            "x_dim": 35,
            "y_dim": 35,
            "n_cities": 4,
            "max_rails_between_cities": 2,
            "max_rail_pairs_in_city": 2,
            "malfunction_rate": 1 / 50,
            "seed": 0
        }
train_env_params = objectview(train_env_params_dict)

eval_env_params_dict = {
            # Test_1
            "n_agents": 20,
            "x_dim": 35,
            "y_dim": 35,
            "n_cities": 4,
            "max_rails_between_cities": 2,
            "max_rail_pairs_in_city": 2,
            "malfunction_rate": 1 / 50,
            "seed": 0
        }
eval_env_params = objectview(eval_env_params_dict)

obs_params_dict = {
        "observation_tree_depth": 2,
        "observation_radius": 10,
        "observation_max_path_depth": 30
    }
obs_params = objectview(obs_params_dict)


# In[ ]:


multi_agent_random_env.train_agent(train_params, train_env_params, eval_env_params, obs_params)

