from datetime import datetime
import os
import random
import sys
from argparse import ArgumentParser, Namespace
from pathlib import Path
from pprint import pprint
from rtpt import RTPT
import random
import time

import psutil
from flatland.utils.rendertools import RenderTool
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import torch

from flatland.envs.step_utils.states import TrainState
from flatland.envs.rail_env import RailEnv, RailEnvActions
from flatland.envs.rail_generators import custom_rail_generator
from flatland.envs.line_generators import custom_line_generator
# from flatland.envs.observations import TreeObsForRailEnv
from flatland.envs.observations import GlobalObsForRailEnv
from flatland.envs.planning_controllers import PlanningController

from flatland.envs.malfunction_generators import ParamMalfunctionGen, MalfunctionParameters
from flatland.envs.predictions import ShortestPathPredictorForRailEnv

base_dir = Path(__file__).resolve().parent.parent
sys.path.append(str(base_dir))

from utils.timer import Timer
from utils.observation_utils import normalize_observation
from reinforcement_learning.dddqn_policy import DDDQNPolicy

try:
    import wandb
    wandb.init(sync_tensorboard=True)
except ImportError:
    print("Install wandb to log to Weights & Biases")

"""
This file shows how to train multiple agents using a reinforcement learning approach.
After training an agent, you can submit it straight away to the Flatland 3 challenge!

Agent documentation: https://flatland.aicrowd.com/tutorials/rl/multi-agent.html
Submission documentation: https://flatland.aicrowd.com/challenges/flatland3/first-submission.html
"""


# def create_rail_env(env_params, tree_observation):
def create_rail_env(env_params):
    n_agents = env_params.n_agents
    x_dim = env_params.x_dim
    y_dim = env_params.y_dim
    n_cities = env_params.n_cities
    max_rails_between_cities = env_params.max_rails_between_cities
    max_rail_pairs_in_city = env_params.max_rail_pairs_in_city
    seed = env_params.seed

    # Break agents from time to time
    malfunction_parameters = MalfunctionParameters(
        malfunction_rate=env_params.malfunction_rate,
        min_duration=20,
        max_duration=50
    )

    return RailEnv(
        width=x_dim, height=y_dim,
        rail_generator=custom_rail_generator(
            max_num_cities=n_cities,
            grid_mode=False,
            max_rails_between_cities=max_rails_between_cities,
            max_rail_pairs_in_city=max_rail_pairs_in_city
        ),
        line_generator=custom_line_generator(),
        number_of_agents=n_agents,
        malfunction_generator=ParamMalfunctionGen(malfunction_parameters),
        # obs_builder_object=tree_observation,
        obs_builder_object=GlobalObsForRailEnv(),
        random_seed=seed
    )


def train_agent(train_params, train_env_params, eval_env_params, obs_params):
    # Environment parameters
    n_agents = train_env_params.n_agents
    x_dim = train_env_params.x_dim
    y_dim = train_env_params.y_dim
    n_cities = train_env_params.n_cities
    max_rails_between_cities = train_env_params.max_rails_between_cities
    max_rail_pairs_in_city = train_env_params.max_rail_pairs_in_city
    seed = train_env_params.seed
    max_nb_conflicts = round(n_agents / 5)

    # Unique ID for this training
    now = datetime.now()
    training_id = now.strftime('%y%m%d%H%M%S')

    # Observation parameters
    # observation_tree_depth = obs_params.observation_tree_depth
    # observation_radius = obs_params.observation_radius
    # observation_max_path_depth = obs_params.observation_max_path_depth

    # Training parameters
    eps_start = train_params.eps_start
    eps_end = train_params.eps_end
    eps_decay = train_params.eps_decay
    n_episodes = train_params.n_episodes
    checkpoint_interval = train_params.checkpoint_interval
    n_eval_episodes = train_params.n_evaluation_episodes
    restore_replay_buffer = train_params.restore_replay_buffer
    save_replay_buffer = train_params.save_replay_buffer

    # Set the seeds
    random.seed(seed)

    # Observation builder
    # predictor = ShortestPathPredictorForRailEnv(observation_max_path_depth)
    # tree_observation = TreeObsForRailEnv(max_depth=observation_tree_depth, predictor=predictor)

    # Setup the environments
    # train_env = create_rail_env(train_env_params, tree_observation)
    train_env = create_rail_env(train_env_params)
    # eval_env = create_rail_env(eval_env_params, tree_observation)
    eval_env = create_rail_env(eval_env_params)

    # Setup renderer
    if train_params.render:
        env_renderer = RenderTool(train_env, gl="PILSVG")

    # Calculate the state size given the depth of the tree observation and the number of features
    # n_features_per_node = train_env.obs_builder.observation_dim
    # n_nodes = sum([np.power(4, i) for i in range(observation_tree_depth + 1)])
    # state_size = n_features_per_node * n_nodes * 2 + 8 * 2 * 3 * 30
    # the state consists of 10 possible resolutions, each contributing 2 trajectories of length 30 with 3
    # coordinates for each waypoint, 1 scalar to indicate the number of follow-up conflicts and information about
    # those consisting of time, agents involved and type of conflict
    state_size = 10 * (2 * 3 * 30 + 1 + max_nb_conflicts * 4)

    # The action space of conflict resolution is 8 (4 waiting options and 4 rerouting options)
    action_size = 10

    # Smoothed values used as target for hyperparameter tuning
    smoothed_normalized_score = -10.0 * n_agents
    smoothed_eval_normalized_score = -10.0 * n_agents
    smoothed_completion = 0.0
    smoothed_eval_completion = 0.0
    maximum_completion = 0
    timer_for_termination = 0
    smoothed_reward = - 15000.0
    smoothed_nb_conflicts = n_agents

    # Double Dueling DQN policy
    policy = DDDQNPolicy(state_size, action_size, train_params)

    # Loads existing replay buffer
    if restore_replay_buffer:
        try:
            policy.load_replay_buffer(restore_replay_buffer)
            policy.test()
        except RuntimeError as e:
            print("\n🛑 Could't load replay buffer, were the experiences generated using the same tree depth?")
            print(e)
            exit(1)

    print("\n💾 Replay buffer status: {}/{} experiences".format(len(policy.memory.memory), train_params.buffer_size))

    hdd = psutil.disk_usage('/')
    if save_replay_buffer and (hdd.free / (2 ** 30)) < 500.0:
        print("⚠️  Careful! Saving replay buffers will quickly consume a lot of disk space. You have {:.2f}gb left.".format(hdd.free / (2 ** 30)))

    # TensorBoard writer
    writer = SummaryWriter()
    writer.add_hparams(vars(train_params), {})
    writer.add_hparams(vars(train_env_params), {})
    writer.add_hparams(vars(obs_params), {})

    training_timer = Timer()
    training_timer.start()

    print("\n🚉 Training {} trains on {}x{} grid for {} episodes, evaluating on {} episodes every {} episodes. Training id '{}'.\n".format(
        n_agents,
        x_dim, y_dim,
        n_episodes,
        n_eval_episodes,
        checkpoint_interval,
        training_id
    ))

    rtpt = RTPT(name_initials='LL', experiment_name='FlatlandDDDQN', max_iterations=800)
    rtpt.start()

    for episode_idx in range(1, n_episodes + 1):

        step_timer = Timer()
        reset_timer = Timer()
        learn_timer = Timer()
        preproc_timer = Timer()
        inference_timer = Timer()

        # Reset environment
        reset_timer.start()
        obs, info = train_env.reset(regenerate_rail=True, regenerate_schedule=True)
        reset_timer.end()
        controller = PlanningController(train_env)

        # Init these values after reset()
        max_steps = train_env._max_episode_steps
        action_count = [0] * action_size
        action_dict = dict()
        # agent_obs = [None] * n_agents
        # agent_prev_obs = [None] * n_agents
        agent_prev_action = [0] * n_agents
        update_values = [False] * max_steps
        state = [None] * max_steps
        action = [0] * max_steps
        last_prev_state = None
        last_action = 0
        last_state = None

        if train_params.render:
            env_renderer.set_new_rail()

        score = 0
        nb_steps = 0
        actions_taken = []
        nb_conflicts = 0

        # Build initial agent-specific observations
        # for agent in train_env.get_agent_handles():
        #    if obs[agent]:
        #        agent_obs[agent] = normalize_observation(obs[agent], observation_tree_depth,
        #        observation_radius=observation_radius)
        #        agent_prev_obs[agent] = agent_obs[agent].copy()

        # Run episode
        for step in range(max_steps):
            inference_timer.start()
            conflicts = controller.find_conflicts(train_env)
            nb_conflicts += len(conflicts)
            for i in range(len(conflicts)):
                solutions = controller.find_solutions(conflicts[i], i, train_env)
                # solution_array = solutions_to_array(solutions, step)
                # state[i] = np.concatenate([agent_obs[conflicts[i][1]], agent_obs[conflicts[i][2]], solution_array])
                state[i] = solutions_to_array(solutions, step, max_nb_conflicts)
                state[i].astype(float)
                action[i] = policy.act(state[i], eps=eps_start)
                j = 0
                while solutions[action[i]] is None and j < 10:
                    action[i] = (action[i] + 1) % action_size
                    j += 1
                if solutions[action[i]] is not None:
                    update_values[i] = True
                    action_count[action[i]] += 1
                    actions_taken.append(action[i])
                    controller.choose_solution(conflicts[i], solutions[action[i]])

            action_dict = controller.act(obs, train_env)
            inference_timer.end()

            # Environment step
            step_timer.start()
            next_obs, all_rewards, done, info = train_env.step(action_dict, step)
            step_timer.end()

            # Render an episode at some interval
            if train_params.render and episode_idx % checkpoint_interval == 0:
                env_renderer.render_env(
                    show=True,
                    frames=False,
                    show_observations=False,
                    show_predictions=False
                )

            all_done = True
            # Update the observations of the next state
            for agent in train_env.get_agent_handles():
                # if obs[agent]:
                #    agent_obs[agent] = normalize_observation(obs[agent], observation_tree_depth,
                #                                             observation_radius=observation_radius)
                if not done[agent]:
                    all_done = False

            # Update replay buffer and train agent
            for i in range(len(conflicts)):
                if update_values[i]:
                    learn_timer.start()
                    policy.step(state[i], action[i], sum(all_rewards.values()),
                                # np.concatenate([agent_obs[conflicts[i][1]], agent_obs[conflicts[i][2]],
                                #                np.full(1440, 0)]).astype(float), all_done)
                                np.full(state_size, 0.0), all_done)
                    learn_timer.end()

                    # agent_prev_obs[agent] = agent_obs[agent].copy()
                    agent_prev_action[agent] = action_dict[agent]
                    last_prev_state = np.copy(state[i])
                    last_action = action[i]
                    # last_state = np.copy(np.concatenate((agent_obs[conflicts[i][1]], agent_obs[conflicts[i][2]],
                    #                                     np.full(1440, 0))).astype(float))
                    last_state = np.full(state_size, 0.0)

            if done['__all__'] and last_prev_state is not None:
                learn_timer.start()
                policy.step(last_prev_state, last_action, sum(all_rewards.values()), last_state, all_done)
                learn_timer.end()

                # agent_prev_obs[agent] = agent_obs[agent].copy()
                agent_prev_action[agent] = action_dict[agent]

            for agent in train_env.get_agent_handles():
                # Preprocess the new observations
                # if next_obs[agent]:
                #    preproc_timer.start()
                #    agent_obs[agent] = normalize_observation(next_obs[agent], observation_tree_depth,
                #    observation_radius=observation_radius)
                #    preproc_timer.end()

                score += all_rewards[agent]

            update_values = [False] * max_steps
            nb_steps = step

            if done['__all__']:
                break

        # Epsilon decay
        eps_start = max(eps_end, eps_decay * eps_start)

        # Collect information about training
        tasks_finished = sum([agent.state == TrainState.DONE for agent in train_env.agents])
        completion = tasks_finished / max(1, train_env.get_num_agents())
        normalized_score = score / max(1, nb_conflicts)

        # if no actions were ever taken possibly due to malfunction and so
        # - `actions_taken` is empty [],
        # - `np.sum(action_count)` is 0
        # Set action probs to count
        if (np.sum(action_count) > 0):
            action_probs = action_count / np.sum(action_count)
        else:
            action_probs = action_count
        action_count = [1] * action_size

        # Set actions_taken to a list with single item 0
        if not actions_taken:
            actions_taken = [0]

        smoothing = 0.99
        smoothed_normalized_score = smoothed_normalized_score * smoothing + normalized_score * (1.0 - smoothing)
        smoothed_completion = smoothed_completion * smoothing + completion * (1.0 - smoothing)
        smoothed_nb_conflicts = smoothed_nb_conflicts * smoothing + nb_conflicts * (1.0 - smoothing)
        smoothed_reward = smoothed_reward * smoothing + score * (1.0 - smoothing)
        if smoothed_completion > maximum_completion:
            timer_for_termination = 0
            maximum_completion = smoothed_completion
        else:
            timer_for_termination += 1

        # Print logs
        if episode_idx % checkpoint_interval == 0:
            torch.save(policy.qnetwork_local, './checkpoints/' + training_id + '-' + str(episode_idx) + '.pth')

            if save_replay_buffer:
                policy.save_replay_buffer('./replay_buffers/' + training_id + '-' + str(episode_idx) + '.pkl')

            if train_params.render:
                env_renderer.close_window()

        print(
            '\r🚂 Episode {}'
            '\t 🏆 Score: {:.3f}'
            ' Avg: {:.3f}'
            '\t 🏆 Reward: {}'
            ' Avg: {:.3f}'
            '\t 💯 Done: {:.2f}%'
            ' Avg: {:.2f}%'
            '\t 🎲 Epsilon: {:.3f} '
            '\t 🔀 Action Probs: {}'.format(
                episode_idx,
                normalized_score,
                smoothed_normalized_score,
                score,
                smoothed_reward,
                100 * completion,
                100 * smoothed_completion,
                eps_start,
                format_action_prob(action_probs)
            ), end=" ")

        # Evaluate policy and log results at some interval
        if episode_idx % checkpoint_interval == 0 and n_eval_episodes > 0:
            scores, completions, nb_steps_eval, rewards, nb_conflicts = eval_policy(eval_env, policy, train_params,
                                                                                    obs_params)

            writer.add_scalar("evaluation/scores_min", np.min(scores), episode_idx)
            writer.add_scalar("evaluation/scores_max", np.max(scores), episode_idx)
            writer.add_scalar("evaluation/scores_mean", np.mean(scores), episode_idx)
            writer.add_scalar("evaluation/scores_std", np.std(scores), episode_idx)
            writer.add_histogram("evaluation/scores", np.array(scores), episode_idx)
            writer.add_scalar("evaluation/rewards_min", np.min(rewards), episode_idx)
            writer.add_scalar("evaluation/rewards_max", np.max(rewards), episode_idx)
            writer.add_scalar("evaluation/rewards_mean", np.mean(rewards), episode_idx)
            writer.add_scalar("evaluation/rewards_std", np.std(rewards), episode_idx)
            writer.add_histogram("evaluation/rewards", np.array(rewards), episode_idx)
            writer.add_scalar("evaluation/nb_conflicts_min", np.min(nb_conflicts), episode_idx)
            writer.add_scalar("evaluation/nb_conflicts_max", np.max(nb_conflicts), episode_idx)
            writer.add_scalar("evaluation/nb_conflicts_mean", np.mean(nb_conflicts), episode_idx)
            writer.add_scalar("evaluation/nb_conflicts_std", np.std(nb_conflicts), episode_idx)
            writer.add_histogram("evaluation/nb_conflicts", np.array(nb_conflicts), episode_idx)
            writer.add_scalar("evaluation/completions_min", np.min(completions), episode_idx)
            writer.add_scalar("evaluation/completions_max", np.max(completions), episode_idx)
            writer.add_scalar("evaluation/completions_mean", np.mean(completions), episode_idx)
            writer.add_scalar("evaluation/completions_std", np.std(completions), episode_idx)
            writer.add_histogram("evaluation/completions", np.array(completions), episode_idx)
            writer.add_scalar("evaluation/nb_steps_min", np.min(nb_steps_eval), episode_idx)
            writer.add_scalar("evaluation/nb_steps_max", np.max(nb_steps_eval), episode_idx)
            writer.add_scalar("evaluation/nb_steps_mean", np.mean(nb_steps_eval), episode_idx)
            writer.add_scalar("evaluation/nb_steps_std", np.std(nb_steps_eval), episode_idx)
            writer.add_histogram("evaluation/nb_steps", np.array(nb_steps_eval), episode_idx)

            smoothing = 0.9
            smoothed_eval_normalized_score = smoothed_eval_normalized_score * smoothing + np.mean(scores) * (1.0 - smoothing)
            smoothed_eval_completion = smoothed_eval_completion * smoothing + np.mean(completions) * (1.0 - smoothing)
            writer.add_scalar("evaluation/smoothed_score", smoothed_eval_normalized_score, episode_idx)
            writer.add_scalar("evaluation/smoothed_completion", smoothed_eval_completion, episode_idx)

        # Save logs to tensorboard
        writer.add_scalar("training/nb_conflicts", nb_conflicts, episode_idx)
        writer.add_scalar("training/smoothed_nb_conflicts", smoothed_nb_conflicts, episode_idx)
        writer.add_scalar("training/reward", score, episode_idx)
        writer.add_scalar("training/smoothed_reward", smoothed_reward, episode_idx)
        writer.add_scalar("training/score", normalized_score, episode_idx)
        writer.add_scalar("training/smoothed_score", smoothed_normalized_score, episode_idx)
        writer.add_scalar("training/completion", np.mean(completion), episode_idx)
        writer.add_scalar("training/smoothed_completion", np.mean(smoothed_completion), episode_idx)
        writer.add_scalar("training/nb_steps", nb_steps, episode_idx)
        writer.add_histogram("actions/distribution", np.array(actions_taken), episode_idx)
        writer.add_scalar("actions/wait1", action_probs[0], episode_idx)
        writer.add_scalar("actions/wait2", action_probs[1], episode_idx)
        writer.add_scalar("actions/wait3", action_probs[2], episode_idx)
        writer.add_scalar("actions/wait4", action_probs[3], episode_idx)
        writer.add_scalar("actions/reroute1", action_probs[4], episode_idx)
        writer.add_scalar("actions/reroute2", action_probs[5], episode_idx)
        writer.add_scalar("actions/reroute3", action_probs[6], episode_idx)
        writer.add_scalar("actions/reroute4", action_probs[7], episode_idx)
        writer.add_scalar("training/epsilon", eps_start, episode_idx)
        writer.add_scalar("training/buffer_size", len(policy.memory), episode_idx)
        writer.add_scalar("training/loss", policy.loss, episode_idx)
        writer.add_scalar("timer/reset", reset_timer.get(), episode_idx)
        writer.add_scalar("timer/step", step_timer.get(), episode_idx)
        writer.add_scalar("timer/learn", learn_timer.get(), episode_idx)
        writer.add_scalar("timer/preproc", preproc_timer.get(), episode_idx)
        writer.add_scalar("timer/total", training_timer.get_current(), episode_idx)

        if timer_for_termination >= 30:
            torch.save(policy.qnetwork_local, './checkpoints/' + training_id + '-' + str(episode_idx) + '.pth')
            break

        rtpt.step()


def format_action_prob(action_probs):
    action_probs = np.round(action_probs, 3)
    actions = ["w1", "w2", "w3", "w4", "r1", "r2", "r3", "r4"]

    buffer = ""
    for action, action_prob in zip(actions, action_probs):
        buffer += action + " " + "{:.3f}".format(action_prob) + " "

    return buffer


def eval_policy(env, policy, train_params, obs_params):
    n_eval_episodes = train_params.n_evaluation_episodes
    n_agents = env.get_num_agents()
    max_nb_conflicts = np.round(n_agents / 5)
    # tree_depth = obs_params.observation_tree_depth
    # observation_radius = obs_params.observation_radius

    scores = []
    completions = []
    nb_steps = []
    rewards = []
    nb_conflicts = []

    for episode_idx in range(n_eval_episodes):
        obs, info = env.reset(regenerate_rail=True, regenerate_schedule=True)
        controller = PlanningController(env)

        max_steps = env._max_episode_steps
        action_dict = dict()
        # agent_obs = [None] * env.get_num_agents()
        state = [None] * max_steps
        action = [0] * max_steps

        score = 0.0
        nb_conflicts_episode = 0

        final_step = 0

        for step in range(max_steps):
            # for agent in env.get_agent_handles():
            #    if obs[agent]:
            #        agent_obs[agent] = normalize_observation(obs[agent], tree_depth=tree_depth,
            #        observation_radius=observation_radius)

            conflicts = controller.find_conflicts(env)
            nb_conflicts_episode += len(conflicts)
            for i in range(len(conflicts)):
                solutions = controller.find_solutions(conflicts[i], i, env)
                # solution_array = solutions_to_array(solutions, step)
                # state[i] = np.concatenate([agent_obs[conflicts[i][1]], agent_obs[conflicts[i][2]], solution_array])
                state[i] = solutions_to_array(solutions, step, max_nb_conflicts)
                state[i].astype(float)
                action[i] = policy.act(state[i], eps=0.0)
                j = 0
                while solutions[action[i]] is None and j < 50:
                    action[i] = np.random.randint(6)
                    j += 1
                if solutions[action[i]] is not None:
                    controller.choose_solution(conflicts[i], solutions[action[i]])
            action_dict = controller.act(obs, env)

            obs, all_rewards, done, info = env.step(action_dict, step)

            for agent in env.get_agent_handles():
                score += all_rewards[agent]

            final_step = step

            if done['__all__']:
                break

        normalized_score = score / max(1, nb_conflicts_episode)
        scores.append(normalized_score)
        rewards.append(score)

        tasks_finished = sum([agent.state == TrainState.DONE for agent in env.agents])
        completion = tasks_finished / max(1, env.get_num_agents())
        completions.append(completion)

        nb_steps.append(final_step)
        nb_conflicts.append(nb_conflicts_episode)

    print("\t✅ Eval: score {:.3f} done {:.1f}%".format(np.mean(scores), np.mean(completions) * 100.0))

    return scores, completions, nb_steps, rewards, nb_conflicts


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-n", "--n_episodes", help="number of episodes to run", default=2500, type=int)
    parser.add_argument("-t", "--training_env_config", help="training config id (eg 0 for Test_0)", default=0, type=int)
    parser.add_argument("-e", "--evaluation_env_config", help="evaluation config id (eg 0 for Test_0)", default=0, type=int)
    parser.add_argument("--n_evaluation_episodes", help="number of evaluation episodes", default=25, type=int)
    parser.add_argument("--checkpoint_interval", help="checkpoint interval", default=100, type=int)
    parser.add_argument("--eps_start", help="max exploration", default=1.0, type=float)
    parser.add_argument("--eps_end", help="min exploration", default=0.01, type=float)
    parser.add_argument("--eps_decay", help="exploration decay", default=0.99, type=float)
    parser.add_argument("--buffer_size", help="replay buffer size", default=int(1e5), type=int)
    parser.add_argument("--buffer_min_size", help="min buffer size to start training", default=0, type=int)
    parser.add_argument("--restore_replay_buffer", help="replay buffer to restore", default="", type=str)
    parser.add_argument("--save_replay_buffer", help="save replay buffer at each evaluation interval", default=False, type=bool)
    parser.add_argument("--batch_size", help="minibatch size", default=128, type=int)
    parser.add_argument("--gamma", help="discount factor", default=0.99, type=float)
    parser.add_argument("--tau", help="soft update of target parameters", default=1e-3, type=float)
    parser.add_argument("--learning_rate", help="learning rate", default=0.5e-4, type=float)
    parser.add_argument("--hidden_size", help="hidden size (2 fc layers)", default=128, type=int)
    parser.add_argument("--update_every", help="how often to update the network", default=8, type=int)
    parser.add_argument("--use_gpu", help="use GPU if available", default=False, type=bool)
    parser.add_argument("--num_threads", help="number of threads PyTorch can use", default=1, type=int)
    parser.add_argument("--render", help="render 1 episode in 100", default=False, type=bool)
    training_params = parser.parse_args()

    env_params = [
        {
            # Test_0
            "n_agents": 5,
            "x_dim": 30,
            "y_dim": 30,
            "n_cities": 2,
            "max_rails_between_cities": 2,
            "max_rail_pairs_in_city": 1,
            "malfunction_rate": 1 / 50,
            "seed": 0
        },
        {
            # Test_1
            "n_agents": 10,
            "x_dim": 30,
            "y_dim": 30,
            "n_cities": 2,
            "max_rails_between_cities": 2,
            "max_rail_pairs_in_city": 2,
            "malfunction_rate": 1 / 100,
            "seed": 0
        },
        {
            # Test_2
            "n_agents": 20,
            "x_dim": 30,
            "y_dim": 30,
            "n_cities": 3,
            "max_rails_between_cities": 2,
            "max_rail_pairs_in_city": 2,
            "malfunction_rate": 1 / 200,
            "seed": 0
        },
    ]

    obs_params = {
        "observation_tree_depth": 2,
        "observation_radius": 10,
        "observation_max_path_depth": 30
    }

    def check_env_config(id):
        if id >= len(env_params) or id < 0:
            print("\n🛑 Invalid environment configuration, only Test_0 to Test_{} are supported.".format(len(env_params) - 1))
            exit(1)


    check_env_config(training_params.training_env_config)
    check_env_config(training_params.evaluation_env_config)

    training_env_params = env_params[training_params.training_env_config]
    evaluation_env_params = env_params[training_params.evaluation_env_config]

    print("\nTraining parameters:")
    pprint(vars(training_params))
    print("\nTraining environment parameters (Test_{}):".format(training_params.training_env_config))
    pprint(training_env_params)
    print("\nEvaluation environment parameters (Test_{}):".format(training_params.evaluation_env_config))
    pprint(evaluation_env_params)
    print("\nObservation parameters:")
    pprint(obs_params)

    os.environ["OMP_NUM_THREADS"] = str(training_params.num_threads)
    train_agent(training_params, Namespace(**training_env_params), Namespace(**evaluation_env_params), Namespace(**obs_params))


def solutions_to_array(solutions, step, max_nb_conflicts):
    result = np.array([])
    for i in range(len(solutions)):
        if solutions[i] is None:
            temp1 = np.full(30 * 3, 0)
            temp2 = np.full(30 * 3, 0)
            temp3 = np.full(1, 0)
            temp4 = np.full(4 * max_nb_conflicts, 0)

        else:
            temp1 = np.empty((30, 3))
            temp2 = np.empty((30, 3))
            temp3 = np.empty(1)
            temp4 = np.empty((max_nb_conflicts, 4))
            for j in range(30):
                if len(solutions[i][2]) <= step + j or solutions[i][2][step+j][0] is None:
                    temp1[j][0] = 0
                    temp1[j][1] = 0
                    temp1[j][2] = 0
                else:
                    temp1[j][0] = solutions[i][2][step+j][0][0]
                    temp1[j][1] = solutions[i][2][step+j][0][1]
                    temp1[j][2] = solutions[i][2][step+j][1]
                if len(solutions[i][3]) <= step + j or solutions[i][3][step+j][0] is None:
                    temp2[j][0] = 0
                    temp2[j][1] = 0
                    temp2[j][2] = 0
                else:
                    temp2[j][0] = solutions[i][3][step+j][0][0]
                    temp2[j][1] = solutions[i][3][step+j][0][1]
                    temp2[j][2] = solutions[i][3][step+j][1]
            temp1 = np.hstack(temp1)
            temp2 = np.hstack(temp2)
            temp3[0] = len(solutions[i][4])
            k = 0
            while k < len(solutions[i][4]):
                temp4[k][0] = solutions[i][4][k][0]
                temp4[k][1] = solutions[i][4][k][1]
                temp4[k][2] = solutions[i][4][k][2]
                temp4[k][3] = solutions[i][4][k][3]
                k += 1
                if k >= max_nb_conflicts:
                    break
            while k < max_nb_conflicts:
                temp4[k][0] = 0
                temp4[k][1] = 0
                temp4[k][2] = 0
                temp4[k][3] = 0
                k += 1
            temp4 = np.hstack(temp4)
        result = np.concatenate([result, temp1, temp2, temp3, temp4])
    return result.astype(float)
