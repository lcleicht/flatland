#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
sys.path.insert(0, "../../../")

from flatland_starter_kit_master.baselines.reinforcement_learning import Multi_Agent_Evaluation


# In[2]:

import os
os.chdir('../../')


#nice trick from https://goodcode.io/articles/python-dict-object/ (accessed on 09.12.2021)
class objectview(object):
    def __init__(self, d):
        self.__dict__ = d

env_params_dict = {
            "n_agents": 50,
            "x_dim": 118,
            "y_dim": 27,
            "n_cities": 3,
            "max_rails_between_cities": 2,
            "max_rail_pairs_in_city": 11,
            "seed": 0,
            "n_episodes": 50,
            "render_episodes": False,
            # evaluation models: 0 = always wait, 1 = heuristic, 2 = double track DN, 3 = double track imitation DN, 4 = small
            # env DN, 5 = small env imitation DN, 6 = medium env DN, 7 = medium env imitation DN, 8 = big env DN, 9 = big
            # env imitation DN, 10 = curriculum DN, 11 = curriculum imitation DN
            "evaluation_model": 11
        }
env_params = objectview(env_params_dict)


# In[3]:


Multi_Agent_Evaluation.evaluate_agents(env_params)


# In[ ]:




